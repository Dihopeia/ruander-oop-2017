<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ruander OOP - PHP tarnfolyam</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <header class="col-xs-12">
                    <h1>Ruander OOP - PHP tanfolyam</h1>
                    
                </header>
                    <div class="row">
                        <article>
                            <?php include('teszt.php'); ?>
                        </article>
                    </div>
                    <footer class="row">
                        <div class="col-md-6 col-xs-12">
                            Minden tartalom védett &copy; <?php date('Y');?>
                        </div>
                        <div class="col-md-6 col-xs-12">
                             Készítette: Horváth Gábor
                        </div>
                    </footer>
            </div>
            
        </div>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>   
    </body>
</html>
